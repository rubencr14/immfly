
from contents.models import Channel, Content
from django.core.files import File
from django.conf import settings
import os

"""
STEPS FOR POPULATING DATABASES:

1. Create the channels
2. Create contents

"""

#clear databases
for elem in Channel.objects.all(): elem.delete()
for elem in Content.objects.all(): elem.delete()

tv_shows = Channel(
    title='TV shows',
    language='en',
    picture=os.path.join(settings.MEDIA_URL, 'images', 'tv-shows.jpg'),
    ignore_check=True
)
movies = Channel(
    title='Movies',
    language='en',
    picture=os.path.join(settings.MEDIA_URL, 'images', 'movies.jpg'),
    ignore_check=True
)

breaking_bad = Channel(
    title='Breaking Bad',
    language='en',
    picture=os.path.join(settings.MEDIA_URL, 'images', 'breaking-bad.jpg'),
    parent=tv_shows,
    ignore_check=True
)
game_thrones = Channel(
    title='Game of Thrones',
    language='en',
    picture=os.path.join(settings.MEDIA_URL, 'images', 'game-thrones.jpg'),
    parent=tv_shows,
    ignore_check=True
)

harry_potter = Content(
    content = '',
    metadata = {
        "title" : "Harry Potter and the Philosopher's Stone",
        "description" : "Harry Potter and the Philosopher's Stone is the first movie in the popular Harry Potter series. It follows the story of young orphan Harry as he discovers he is a wizard and attends Hogwarts School of Witchcraft and Wizardry. Along with his friends Ron and Hermione, they try to stop the evil wizard Voldemort from stealing the Philosopher's Stone. The movie is full of adventure, magic, and humor and features a talented cast of actors",
        "image" : os.path.join(settings.MEDIA_URL, 'images', 'harry-potter.jpg'),
        "year" : 2001,
        "duration" : 120,
        "genre" : "fantasy"
    },
    rating_value = 7.2,
    channel = movies
)

lord_rings = Content(
    content = '',
    metadata = {
        "title" : "Lord of the rings",
        "description" : "The Lord of the Rings is a high-fantasy novel written by J.R.R. Tolkien. It follows the journey of a hobbit named Frodo Baggins as he sets out to destroy the One Ring, a powerful and evil artifact created by the dark lord Sauron. Along with a fellowship of other creatures, including elves, dwarves, and humans, Frodo faces many challenges and battles as they journey through Middle-earth to reach the fiery depths of Mount Doom, where the ring must be destroyed. The novel explores themes of heroism, friendship, and the struggle between good and evil, and has become a classic of modern literature",
       "image" : os.path.join(settings.MEDIA_URL, 'images', 'lord-rings.jpg'),
       "year" : 2001,
        "duration" : 142,
        "genre" : "fantasy"
    },
    rating_value = 8.3,
    channel = movies
)

breaking_bad_1 = Content(
    content = '',
    metadata = {
        "title" : "Breaking Bad - Episode 1",
        "description" : ""
    },
    rating_value = 9.3,
    channel = breaking_bad
)

breaking_bad_2 = Content(
    content = '',
    metadata = {
        "title" : "Breaking Bad - Episode 2",
        "description" : ""
    },
    rating_value = 8.8,
    channel = breaking_bad
)

breaking_bad_3 = Content(
    content = '',
    metadata = {
        "title" : "Breaking Bad - Episode 3",
        "description" : ""
    },
    rating_value = 7.9,
    channel = breaking_bad
)

game_thrones_1 = Content(
    content = '',
    metadata = {
        "title" : "Game of Thrones - Episode 1",
        "description" : ""
    },
    rating_value = 9.1,
    channel = game_thrones
)

game_thrones_2 = Content(
    content = '',
    metadata = {
        "title" : "Game of Thrones - Episode 2",
        "description" : ""
    },
    rating_value = 9.5,
    channel = game_thrones
)

game_thrones_3 = Content(
    content = '',
    metadata = {
        "title" : "Game of Thrones - Episode 3",
        "description" : ""
    },
    rating_value = 7.7,
    channel = game_thrones
)

tv_shows.save()
movies.save()

breaking_bad.save()
game_thrones.save()

harry_potter.save()
lord_rings.save()

breaking_bad_1.save()
breaking_bad_2.save()
breaking_bad_3.save()

game_thrones_1.save()
game_thrones_2.save()
game_thrones_3.save()

#how to check all the tv shows
tv_shows = Channel.objects.get(title='TV shows')
shows = Channel.objects.filter(parent=tv_shows)

#check contents have a channel
movies_channel = Channel.objects.get(title='Movies')
contents_channel = Content.objects.filter(channel=movies_channel)

