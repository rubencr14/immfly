import os
import pandas as pd
from django.conf import settings
from django.test import TestCase
from contents.models import Channel, Content
from utils.populate_db_test import populate_test_database
from django.core.management import call_command
from utils.rating import (compute_rating,
                          compute_channel_rating_from_content, 
                          compute_channel_rating_from_subchannels)

class RatingAlgorithmTest(TestCase):

    def setUp(self):
        populate_test_database()

    def test_compute_channel_rating_from_content(self):
        channel = Channel.objects.get(title='Movies test')
        rating = compute_channel_rating_from_content(channel, Content)
        self.assertEqual(rating, 7.75)

    def test_compute_channel_rating_from_subchannels(self):
        channel = Channel.objects.get(title='TV shows test')
        rating = compute_channel_rating_from_subchannels(channel, Channel, Content)
        self.assertEqual(rating, 8.72)

    def test_compute_rating(self):
        rating_mov = compute_rating(Channel, Content, 'Movies test')
        rating_show = compute_rating(Channel, Content, 'TV shows test')
        self.assertEqual(rating_mov, 7.75)
        self.assertEqual(rating_show, 8.72)

    def test_management_command(self):
        csv_path = os.path.join(settings.MEDIA_ROOT, 'docs', 'channel_rating.csv')
        if os.path.isfile(csv_path):
            os.remove(csv_path)
        call_command('calculateratings')
        self.assertTrue(os.path.isfile(csv_path))
        df = pd.read_csv(csv_path)
        self.assertEqual(df.columns.tolist(), ['channel title', 'average rating'])
        ratings = df['average rating'].tolist()
        ratings_sorted = sorted(ratings, reverse=True)
        self.assertEqual(ratings, list(ratings_sorted))

