
def compute_channel_rating_from_content(channel, content_obj):
    contents = content_obj.objects.filter(channel=channel)
    total_count = contents.count()
    total_sum = sum([content.rating_value for content in contents])
    return round(total_sum / total_count, 2)

def compute_channel_rating_from_subchannels(channel, channel_obj, content_obj):
    subchannels = channel_obj.objects.filter(parent=channel)
    rating_arr = []
    for subchannel in subchannels:
        rating = compute_channel_rating_from_subchannels(subchannel, channel_obj, content_obj) if subchannel.has_subchannels() else \
                 compute_channel_rating_from_content(subchannel, content_obj)
        rating_arr.append(rating)
    return round(sum(rating_arr) / len(rating_arr), 2)

def compute_rating(channel_obj, content_obj, channel_title):
    channel = channel_obj.objects.get(title=channel_title)
    return compute_channel_rating_from_subchannels(channel, channel_obj, content_obj) \
            if channel.has_subchannels() else compute_channel_rating_from_content(channel, content_obj)