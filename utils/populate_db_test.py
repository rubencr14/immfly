import os
from django.conf import settings
from contents.models import Channel, Content

def populate_test_database():

    tv_shows = Channel(
    id=1,
    title='TV shows test',
    language='en',
    picture=os.path.join(settings.MEDIA_URL, 'images', 'tv-shows.jpg'),
    ignore_check=True
    )
    movies = Channel(
        id=2,
        title='Movies test',
        language='en',
        picture=os.path.join(settings.MEDIA_URL, 'images', 'movies.jpg'),
        ignore_check=True
    )

    breaking_bad = Channel(
        id=3,
        title='Breaking Bad subtest',
        language='en',
        picture=os.path.join(settings.MEDIA_URL, 'images', 'breaking-bad.jpg'),
        parent=tv_shows,
        ignore_check=True
    )
    game_thrones = Channel(
        id=4,
        title='Game of Thrones subtest',
        language='en',
        picture=os.path.join(settings.MEDIA_URL, 'images', 'game-thrones.jpg'),
        parent=tv_shows,
        ignore_check=True
    )

    harry_potter = Content(
        id=1,
        content = '',
        metadata = {
            "title" : "Harry Potter and the Philosopher's Stone test",
            "description" : "Test description",
            "image" : os.path.join(settings.MEDIA_URL, 'images', 'harry-potter.jpg'),
            "year" : 2001,
            "duration" : 120,
            "genre" : "fantasy"
        },
        rating_value = 7.2,
        channel = movies
    )

    lord_rings = Content(
        id=2,
        content = '',
        metadata = {
            "title" : "Lord of the rings",
            "description" : "Test description",
        "image" : os.path.join(settings.MEDIA_URL, 'images', 'lord-rings.jpg'),
        "year" : 2001,
            "duration" : 142,
            "genre" : "fantasy"
        },
        rating_value = 8.3,
        channel = movies
    )

    breaking_bad_1 = Content(
        id=3,
        content = '',
        metadata = {
            "title" : "Breaking Bad - Episode 1 test",
            "description" : ""
        },
        rating_value = 9.3,
        channel = breaking_bad
    )

    breaking_bad_2 = Content(
        id=4,
        content = '',
        metadata = {
            "title" : "Breaking Bad - Episode 2 test",
            "description" : ""
        },
        rating_value = 8.8,
        channel = breaking_bad
    )

    breaking_bad_3 = Content(
        id=5,
        content = '',
        metadata = {
            "title" : "Breaking Bad - Episode 3 test",
            "description" : ""
        },
        rating_value = 7.9,
        channel = breaking_bad
    )

    game_thrones_1 = Content(
        id=6,
        content = '',
        metadata = {
            "title" : "Game of Thrones - Episode 1 test",
            "description" : ""
        },
        rating_value = 9.1,
        channel = game_thrones
    )

    game_thrones_2 = Content(
        id=7,
        content = '',
        metadata = {
            "title" : "Game of Thrones - Episode 2 test",
            "description" : ""
        },
        rating_value = 9.5,
        channel = game_thrones
    )

    game_thrones_3 = Content(
        id=8,
        content = '',
        metadata = {
            "title" : "Game of Thrones - Episode 3 test",
            "description" : ""
        },
        rating_value = 7.7,
        channel = game_thrones
    )

    tv_shows.save()
    movies.save()

    breaking_bad.save()
    game_thrones.save()

    harry_potter.save()
    lord_rings.save()

    breaking_bad_1.save()
    breaking_bad_2.save()
    breaking_bad_3.save()

    game_thrones_1.save()
    game_thrones_2.save()
    game_thrones_3.save()