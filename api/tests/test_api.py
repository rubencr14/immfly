from django.test import TestCase
from rest_framework.test import APIClient
from contents.models import Channel, Content
from rest_framework import status
from django.conf import settings
from utils.populate_db_test import populate_test_database
import json
# Create your tests here.

class ChannelApiTest(TestCase):

    def setUp(self):
        populate_test_database()
        self.client = APIClient()

    def test_get_list_channels(self):
        response = self.client.get('/api/channels/')
        db_list = [{'id': 1, 'title': 'TV shows test', 'language': 'en', 'picture': '/media/media/images/tv-shows.jpg', 'parent': None}, {'id': 2, 'title': 'Movies test', 'language': 'en', 'picture': '/media/media/images/movies.jpg', 'parent': None}, {'id': 3, 'title': 'Breaking Bad subtest', 'language': 'en', 'picture': '/media/media/images/breaking-bad.jpg', 'parent': 1}, {'id': 4, 'title': 'Game of Thrones subtest', 'language': 'en', 'picture': '/media/media/images/game-thrones.jpg', 'parent': 1}]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), db_list)

    def test_get_channel(self):
        response = self.client.get('/api/channels/1/')
        chan_obj = {'id': 1, 'title': 'TV shows test', 'language': 'en', 'picture': '/media/media/images/tv-shows.jpg', 'parent': None}
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), chan_obj)

    def test_post_channel(self):
        client = APIClient()
        obj = {
                'title' : 'TV shows post test',
                'language' : 'en',
                'parent' : ''
            }
        response = client.post('/api/channels/', obj)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_put_channel(self):
        mod_obj_fields = {
                'title' : 'TV shows put test',
                'language' : 'es',
                'parent' : '',
                'picture' : ''
            }
        obj_id = Channel.objects.get(title='TV shows test').id
        response = self.client.put(f'/api/channels/{obj_id}/', mod_obj_fields)
        obj = Channel.objects.get(id=obj_id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(obj.title, mod_obj_fields['title'])
        self.assertEqual(obj.language, mod_obj_fields['language'])
        self.assertEqual(obj.parent, None)
        self.assertEqual(obj.picture, mod_obj_fields['picture'])


    def test_patch_channel(self):
        mod_obj_field = {
            'title' : 'TV shows patch test'
        }
        obj_id = Channel.objects.get(title='TV shows test').id
        response = self.client.patch(f'/api/channels/{obj_id}/', mod_obj_field)
        obj = Channel.objects.get(id=obj_id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(obj.title, mod_obj_field['title'])

    def test_delete_channel(self):
        response = self.client.delete('/api/channels/1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(Channel.DoesNotExist):
            Channel.objects.get(id=1)


class ContentApiTest(TestCase):

    def setUp(self):
        populate_test_database()
        self.client = APIClient()

    def test_get_list_contents(self):
        response = self.client.get('/api/contents/')
        db_list = [{'id': 1, 'content': None, 'metadata': {'year': 2001, 'genre': 'fantasy', 'image': '/media/images/harry-potter.jpg', 'title': "Harry Potter and the Philosopher's Stone test", 'duration': 120, 'description': 'Test description'}, 'rating_value': 7.2, 'channel': 2}, {'id': 2, 'content': None, 'metadata': {'year': 2001, 'genre': 'fantasy', 'image': '/media/images/lord-rings.jpg', 'title': 'Lord of the rings', 'duration': 142, 'description': 'Test description'}, 'rating_value': 8.3, 'channel': 2}, {'id': 3, 'content': None, 'metadata': {'title': 'Breaking Bad - Episode 1 test', 'description': ''}, 'rating_value': 9.3, 'channel': 3}, {'id': 4, 'content': None, 'metadata': {'title': 'Breaking Bad - Episode 2 test', 'description': ''}, 'rating_value': 8.8, 'channel': 3}, {'id': 5, 'content': None, 'metadata': {'title': 'Breaking Bad - Episode 3 test', 'description': ''}, 'rating_value': 7.9, 'channel': 3}, {'id': 6, 'content': None, 'metadata': {'title': 'Game of Thrones - Episode 1 test', 'description': ''}, 'rating_value': 9.1, 'channel': 4}, {'id': 7, 'content': None, 'metadata': {'title': 'Game of Thrones - Episode 2 test', 'description': ''}, 'rating_value': 9.5, 'channel': 4}, {'id': 8, 'content': None, 'metadata': {'title': 'Game of Thrones - Episode 3 test', 'description': ''}, 'rating_value': 7.7, 'channel': 4}]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), db_list)

    def test_get_content(self):
        response = self.client.get('/api/contents/1/')
        cont_obj = {'id': 1, 'content': None, 'metadata': {'year': 2001, 'genre': 'fantasy', 'image': '/media/images/harry-potter.jpg', 'title': "Harry Potter and the Philosopher's Stone test", 'duration': 120, 'description': 'Test description'}, 'rating_value': 7.2, 'channel': 2}
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), cont_obj)

    def test_post_content(self):
        obj = {
                'content' : None,
                'metadata' : {
                    "title" : "Game of Thrones - Episode 3 post test",
                    "description" : ""
                },
                'rating_value' : '7.7',
                'channel' : ''
                }
        response = self.client.post('/api/contents/', obj, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_put_content(self):
        mod_obj_fields = {
                'content' : None,
                'metadata' : {
                    'title' : 'test put content',
                    'description' : 'this is a test for testing put api content'
                },
                'rating_value' : 3.2,
                'channel' : 2
            }
        obj_id = Content.objects.get(metadata__title='Lord of the rings').id
        response = self.client.put(f'/api/contents/{obj_id}/', mod_obj_fields, format='json')
        obj = Content.objects.get(id=obj_id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(obj.metadata, mod_obj_fields['metadata'])
        self.assertEqual(obj.rating_value, mod_obj_fields['rating_value'])
        self.assertEqual(obj.channel, Channel.objects.get(id=2))

    def test_patch_content(self):
        obj_id = Content.objects.get(metadata__title='Lord of the rings').id
        mod_obj_field = {
            'metadata' : {
                'title' : 'test patch content',
                'description' : 'this is a test for testing patch api content'
            }
        }
        response = self.client.patch(f'/api/contents/{obj_id}/', mod_obj_field, format='json')
        obj = Content.objects.get(id=obj_id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(obj.metadata, mod_obj_field['metadata'])

    def test_delete_content(self):
        response = self.client.delete('/api/contents/1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(Content.DoesNotExist):
            Content.objects.get(id=1)

