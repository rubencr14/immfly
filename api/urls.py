from rest_framework import routers
from api.views import ChannelsViewSet, ContentsViewSet

router = routers.DefaultRouter()

router.register('api/channels', ChannelsViewSet, 'channels')
router.register('api/contents', ContentsViewSet, 'contents')

urlpatterns = router.urls