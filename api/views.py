from django.shortcuts import get_object_or_404
from rest_framework_api_key.permissions import HasAPIKey
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, permissions
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from contents.models import Channel, Content
from api.serializers import ChannelSerializer, ContentSerializer

# Create your views here.
class ChannelsViewSet(viewsets.ViewSet):
    
    serializer_class = ChannelSerializer
    #permission_classes = [HasAPIKey | IsAuthenticated]

    def _get_object(self, pk=None):
        queryset = Channel.objects.all()
        return get_object_or_404(queryset, pk=pk)

    def list(self, request):
        queryset = Channel.objects.all()
        serializer = ChannelSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = ChannelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        obj = self._get_object(pk=pk)
        serializer = ChannelSerializer(obj)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        obj = self._get_object(pk=pk)
        serializer = ChannelSerializer(obj, data=request.data)
        serializer.is_valid()
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, pk=None):
        obj = self._get_object(pk=pk)
        serializer = ChannelSerializer(obj, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        obj = self._get_object(pk=pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['get'])
    def subchannels(self, request, pk=None):
        # define the custom action
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ContentsViewSet(viewsets.ViewSet):

    serializer_class = ContentSerializer

    def _get_object(self, pk=None):
        queryset = Content.objects.all()
        return get_object_or_404(queryset, pk=pk)
    
    def list(self, request):
        queryset = Content.objects.all()
        serializer = ContentSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = ContentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        obj = self._get_object(pk=pk)
        serializer = ContentSerializer(obj)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        obj = self._get_object(pk=pk)
        serializer = ContentSerializer(obj, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, pk=None):
        obj = self._get_object(pk=pk)
        serializer = ContentSerializer(obj, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        obj = self._get_object(pk=pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)