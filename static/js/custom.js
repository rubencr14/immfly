
function getEpisodesFromName(title){

    $.ajax({
        type: "POST",
        url: "/get-episodes-from-name",
        data: {"name" : title},
        success: function(response){
          if(response.status==='ok'){
            for(let episode of JSON.parse(response.episodes)){
                const li = document.createElement('li')
                li.innerText = episode.fields.metadata.title
                $('#episodes-list').append(li)
            }
          }
          else{
            alert('error')
          }
        },
        error: function(xhr, status, error){
          console.error(error);
        }
      });
}

function createContentInfo(elem){

    const cont = document.createElement('div')
    cont.className = 'content-info'
    const description = elem.getAttribute('data-description')
    const image_path = elem.getAttribute('data-image')
    const title = elem.getAttribute('data-title')
    const rating = elem.getAttribute('data-rating')
    const year = elem.getAttribute('data-year')
    const duration = elem.getAttribute('data-duration')
    const genre = elem.getAttribute('data-genre')
    const html = `
                <span class="close-pop-up--btn" onclick="closePopUp(this)"><i class="fa fa-times"></i></span>
                <div class="row align-items-center position-relative">
                    <div class="content-dark--bg"></div>
                    <div class="col-xl-3 col-lg-4">
                        <div class="movie-details-img">
                            <img src="${image_path}" alt="">
                            <a href="https://www.youtube.com/watch?v=R2gbPxeNk2E" class="popup-video"><img src="img/images/play_icon.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-8">
                        <div class="movie-details-content">
                            <h2>${title}</h2>
                            <div class="banner-meta">
                                <ul>
                                    <li class="quality">
                                        <span>Pg 18</span>
                                        <span>hd</span>
                                    </li>
                                    <li class="category">
                                        <a href="#">${genre}</a>
                                    </li>
                                    <li class="release-time">
                                        <span><i class="far fa-calendar-alt"></i> ${year}</span>
                                        <span><i class="far fa-clock"></i> ${duration} min</span>
                                        <span><i class="fa fa-star"></i> ${rating}</span>
                                    </li>
                                </ul>
                            </div>
                            <p>${description}</p>
                            <div class="movie-details-prime">
                                <ul>
                                    <li class="share"><a href="#"><i class="fas fa-share-alt"></i> Share</a></li>
                                    <li class="streaming">
                                        <h6>PyFly</h6>
                                        <span>Streaming Channels</span>
                                    </li>
                                    <li class="watch"><a href="#" class="btn popup-video"><i class="fas fa-play"></i> Watch Now</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
    `
    cont.innerHTML = html;
    cont.style.display = 'none'
    document.getElementsByTagName('body')[0].appendChild(cont)
    $(cont).slideDown()
}


function createChannelInfo(elem){

    const cont = document.createElement('div')
    cont.className = 'content-info'
    const description = elem.getAttribute('data-description')
    const image_path = elem.getAttribute('data-image')
    const title = elem.getAttribute('data-title')
    const rating = elem.getAttribute('data-rating')
    const year = elem.getAttribute('data-year')
    const duration = elem.getAttribute('data-duration')
    const genre = elem.getAttribute('data-genre')
    const html = `
                <span class="close-pop-up--btn" onclick="closePopUp(this)"><i class="fa fa-times"></i></span>
                <div class="row align-items-center position-relative">
                    <div class="content-dark--bg"></div>
                    <div class="col-xl-3 col-lg-4">
                        <div class="movie-details-img">
                            <img src="${image_path}" alt="">
                            <a href="https://www.youtube.com/watch?v=R2gbPxeNk2E" class="popup-video"><img src="img/images/play_icon.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-8">
                        <div class="movie-details-content">
                            <h5>Episodes</h5>
                            <h2>${title}</h2>
                            <div class="banner-meta">
                                <ul>
                                    <li class="quality">
                                        <span>Pg 18</span>
                                        <span>hd</span>
                                    </li>
                                    <li class="category">
                                        <a href="#">${genre}</a>
                                    </li>
                                    <li class="release-time">
                                        <span><i class="far fa-calendar-alt"></i> ${year}</span>
                                        <span><i class="far fa-clock"></i> ${duration} min</span>
                                        <span><i class="fa fa-star"></i> ${rating}</span>
                                    </li>
                                </ul>
                            </div>
                            <ul id="episodes-list">
                            
                            </ul>
                            <div class="movie-details-prime">
                                <ul>
                                    <li class="share"><a href="#"><i class="fas fa-share-alt"></i> Share</a></li>
                                    <li class="streaming">
                                        <h6>PyFly</h6>
                                        <span>Streaming Channels</span>
                                    </li>
                                    <li class="watch"><a href="#" class="btn popup-video"><i class="fas fa-play"></i> Watch Now</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
    `
    cont.innerHTML = html;
    cont.style.display = 'none'
    document.getElementsByTagName('body')[0].appendChild(cont)
    getEpisodesFromName(title)
    $(cont).slideDown()
}


function closePopUp(elem){
    $(elem).parent().slideUp()
    $(elem).parent().remove()
}

