# Immfly Technical Test

![alt text](./media/images/docs/pyfly-1.png)

This is a technical test for the Immfly company, which involves creating an API for a media platform that displays content in a hierarchical structure. 

The API must include models to represent the content structure, endpoints to retrieve channels, subchannels, and contents, and a management command to calculate the ratings of each channel and export them to a CSV file sorted by rating.

## Project Details

In this section we will shortly review some details of the implementation of this project.

### Backend 

For this project I have included a backend created with the typical **Django MVT (Model-View-Template)** scheme. 

The internal endpoints have been created using views.py for url-method relationship and urls.py for routing. 

The **external API have been coded using the Django Rest Framework** which allows to create an API following the REST principles.

### Frontend

A **simple Frontend has been also implemented** as a demo. HTML, CSS and javascript have been used. It can be accesed locally in http://localhost:8000 when the containers are running (see next section).

![alt text](./media/images/docs/pyfly-1.png)
![alt text](./media/images/docs/pyfly-2.png)
![alt text](./media/images/docs/pyfly-3.png)
![alt text](./media/images/docs/pyfly-4.png)

### Launching the project

This project can be launched in several ways. It can be run through docker following the next commands:

Sometimes, the **following command is necessary**:

```console
chmod +x entrypoint.sh
```

Then we can **build images and run containers**:

```console
docker-compose up -d
```

This command will build the database image and Django image and will run the containers. The Django internal port will be mapped externally through the 8000 port. 

Now **you can access the webapp demo** in your browser: http://localhost:8000

**Unit tests can be ran** inside the docker container. This can be done accessing the terminal using Docker Desktop or directly in the console with the following commands. 

```console
docker ps
```

With this command we will get the **id of the immfly-web container**. We can use this ID for accessing the container console:

```console
docker exec -it {CONTAINER_ID} /bin/bash
```

Now we can **run the unit tests**:

```console
python manage.py test
```

Also we can **create the ratings csv file** using the following command:

```console
python manage.py calculateratings
```

The csv file will be saved in: **/immfly/media/docs/channel_rating.csv**


This project can also be run locally configuring manually the database and running the server using python manage.py runserver. 

Also, the environment variables have to be set and the database migrations ran using python manage.py makemigrations and python manage.py migrate.

However, this can take much longer and probably there will be dependency problems.

> **I highly recommend to use the first option (Docker) since it will be much easier.**

## Database

The database used for this project is a **relational MySQL** database used through the ORM of Django (Object Relational Mapper) since there are some **relations between channels, subchannels and contents**.

Other databases such as MongoDB could have been used, for example, to store content metadata. 

The implementation of details will depend on the needs of the project.

The relations between models **have been designed as follows**:

![alt text](./media/images/docs/Immfly_db.png)

Channels have a one-to-many relationship with itself since a Channel (for example a TV show) can have subchannels (for example Breaking Bad).

Each Channel can have a one-to-many relationship with contents, since a Channel (for example movies) can have multiple movie contents.

All this design are found in models.py inside the contents directory.

## API

An API have been created using the DRF (Django Rest Framework).

**The endpoints are the followings:**

- Get
    - /channels/
    - /channels/{channel_id}
    - /contents/
    - /contents/{content_id}
- Post
    - /channels/{channel_id}
    - /contents/{content_id}
- Put
    - /channels/{channel_id}
    - /contents/{content_id}
- Patch
    - /channels/{channel_id}
    - /contents/{content_id}
- Delete
    - /channels/{channel_id}
    - /contents/{content_id}

The API documentation can be accessed through http://localhost:8000/swagger/ if the web app is running locally.
![alt text](./media/images/docs/swagger-doc.png)

For example, you can get the list of channels using the following command: 

```console
curl -X 'GET' 'http://localhost:8000/api/channels/' -H 'accept: application/json' -H 'X-CSRFToken: MiiexOYjDeVRO9X4v59m6VuHLMK5RDU87CTI6ScO84vMEmBpmnmd1jMOhjjcYWDp'
```

or using python:

```python
import requests
url = 'http://localhost:8000/api/channels/'
response = requests.get(url)
print(response.json())
```

## Calculate Ratings

To calculate the ratings of the channels from subchannels and contents taking into account the hierarchy some functions have been implemented in the utils/rating.py file.

The **rating can be calculated** using the next Django Management Command:

```console
python manage.py calculateratings
```

A csv file is created in /media/docs/channel_rating.csv path. 

It contains the rating of each channel sorted by rating value.

## Testing

Several unit tests have been implemented. The different tests can be found in each app directory in the tests modules:

- contents/tests
- utils/tests
- api/tests

Before each test, a test database is created in order not to change the real database.

After each test it is destroyed.

### Contents

In contents we find the tests regarding the models. These unitary tests make sure the the hierarchy is accomplished and that the fields are correct.

More checks can be done using unit tests for contents such as tests for the views. Nonetheless, since it is a short technical project we have considered only some of them.

### Utils

The tests in utils basically check the rating algorithm. It makes sure that each channel's rating will be computed properly either it it has subchannels or contents.

It also checks that the management command:

```console
python manage.py calculateratings
```

will perform properly and will create the csv file in the /media/docs directory sorted by ratings (ascending).

### API

Api tests have also been included. This unit tests use APIClient class from rest_framework to create a client that can make requests to the API. 

GET, POST, PUT, PATCH and DELETE HTTP methods have been tested for both: channels and contents endpoints.

### Future Considerations

We have only included some of the possible tests regarding the scope of this project.

However, further tests can be written. More unit tests for also **integration tests** to see how the components interact and also **functional tests**.

Apart from this, **Frontend (UI) tests** can also be included. To simulate how users interact **Selenium** or **WebdriverIO** can be utilized.

As we will discuss in the CI/CD part other tools such as code-review tools (for example, SonarQube) can also be used for assessing the quality of the code.

## CI/CD

An example of CI pipeline have been implemented. Only two stages have been included in this project:

- build
- test

Nonetheless, other stages can be further implemented. For instance, there could be also a deployment stage for continous deployment after all tests have been passed.

Gitlab pipelines are used for this project although Jenkins or Github Actions could also been used. 

Each time a commit is pushed to the repo, this pipeline is created. Docker images are built and tests are ran. 

Another way of doing CI/CD would be when merging to the main branch using a pull request before deployment. 
![alt text](./media/images/docs/gitlab-ci-image.png)

## Proposed Future Changes

There are many improvements that could be made in this project that due to lack of time have not been carried out. Some of them are the following:

1. Add more tests to increase coverage. We could also use tools like SonarQube to get an idea of coverage as well as code quality through code smells detection.

2. We could also add in the CI/CD pipeline a part for format and style checking. That is, make sure that the code follows the PEP8 standards using a tool like flake8.

3. During the Gitlab pipeline, the deploy "stage" could also be added to directly deploy the project once the format and testing checks have been passed.

4. We could add "type hints" in Python using, for example, the Typing library and then check using MyPy.

5. We could also add more security to the API using, for example, the Django Rest Framework Api Key library. This would allow us to create api keys that last for a specific time and that the endpoints of this API can only be accessed by passing the Api Key in the request headers.