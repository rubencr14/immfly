#!/bin/bash

# Running Django migrations to update the database
python manage.py makemigrations
python manage.py migrate

# Run the script to populate the database with initial data
python manage.py shell < utils/populate_db.py

# Start the Django server
python manage.py runserver 0.0.0.0:8000