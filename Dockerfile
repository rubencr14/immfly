FROM python:3.11

ENV PYTHONUNBUFFERED 1

WORKDIR /immfly

COPY requirements.txt /immfly/
RUN pip install -r requirements.txt

COPY . /immfly/

# RUN mv /immfly/.env-docker /immfly/.env

EXPOSE 8000