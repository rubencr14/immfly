from django.urls import path
from contents import views
from django.contrib.sitemaps.views import sitemap

urlpatterns = [
    path('', views.home),
    path('get-episodes-from-name', views.get_episodes_from_name)
] 