from django.db import models
from django.conf import settings
from contents.errors import SubchannelsOrContentsNotFoundError
from utils.rating import compute_rating
# Create your models here.

class Content(models.Model):
    content = models.FileField(blank=True, null=True)
    metadata = models.JSONField()
    rating_value = models.FloatField() #or Decimal Field
    channel = models.ForeignKey('Channel', on_delete=models.CASCADE, null=True, blank=True)

class Channel(models.Model):

    title = models.CharField(max_length=255)
    language = models.CharField(max_length=255)
    picture = models.ImageField(upload_to='images', null=True, blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)


    def __init__(self, *args, **kwargs):
        ignore_check = kwargs.get('ignore_check', False)
        kwargs.pop('ignore_check', '')
        super().__init__(*args, **kwargs)
        if not ignore_check:
            self._check_if_channel_has_subchannels_or_contents()

    @property
    def rating(self):
        return compute_rating(Channel, Content, self.title)

    def has_subchannels(self):
        return Channel.objects.filter(parent=self).count() > 0

    def has_contents(self):
        return Content.objects.filter(channel=self).count() > 0

    def _check_if_channel_has_subchannels_or_contents(self):
        """
        This function is for checking if subchannels or contents
        exists for a given channel. Else it raises error
        """
        if(not self.has_subchannels()) and (not self.has_contents()):
            raise SubchannelsOrContentsNotFoundError('A channel must have subchannels or contents')