from django.test import TestCase
from contents.models import Channel, Content
from django.conf import settings
from utils.populate_db_test import populate_test_database
from contents.errors import SubchannelsOrContentsNotFoundError
from utils.rating import compute_rating
import os

class ChannelModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        populate_test_database()
    
    def test_no_contents_or_subchannels_error(self):
        chan = Channel(
                title='Music test',
                language='en',
                picture=os.path.join(settings.MEDIA_URL, 'images', 'tv-shows.jpg')
            )
        chan.save()
        with self.assertRaises(SubchannelsOrContentsNotFoundError) as err:
            Channel.objects.get(title='Music test')
        self.assertEqual("A channel must have subchannels or contents", str(err.exception))

    def test_hierarchy(self):
        channel = Channel.objects.get(title='TV shows test')
        subchannels =  Channel.objects.filter(title__contains='subtest')
        self.assertGreater(subchannels.__len__(), 0, subchannels)
        for subchannel in subchannels:
            self.assertEqual(subchannel.parent, channel)

    def test_has_subchannels(self):
        chan = Channel.objects.get(title='TV shows test')
        self.assertTrue(chan.has_subchannels())

    def test_has_subcontents(self):
        chan = Channel.objects.get(title='Movies test')
        self.assertTrue(chan.has_contents())

    def test_has_no_subchannels(self):
        chan = Channel.objects.get(title='Movies test')
        self.assertFalse(chan.has_subchannels())

    def test_has_no_subcontents(self):
        chan = Channel.objects.get(title='TV shows test')
        self.assertFalse(chan.has_contents())

    def test_title_max_length(self):
        chan = Channel.objects.get(title='TV shows test')
        max_len = chan._meta.get_field('title').max_length
        self.assertEqual(max_len, 255)

    def test_language_max_length(self):
        chan = Channel.objects.get(title='TV shows test')
        max_len = chan._meta.get_field('language').max_length
        self.assertEqual(max_len, 255)

    def test_channel_rating(self):
        chan = Channel.objects.get(title='TV shows test')
        rating = compute_rating(Channel, Content, 'TV shows test')
        self.assertEqual(rating, chan.rating)


class ContentModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        populate_test_database()

    def test_has_parent(self):
        content = Content.objects.get(metadata__title='Breaking Bad - Episode 3 test')
        chan = Channel.objects.get(title='Breaking Bad subtest')
        self.assertEqual(content.channel, chan)
