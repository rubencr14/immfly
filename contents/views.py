from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from contents.models import Channel, Content
from django.core import serializers

# Create your views here.
def home(request):
    movies = Content.objects.filter(channel=Channel.objects.get(title='Movies'))
    shows = Channel.objects.filter(parent=Channel.objects.get(title='TV shows'))
    return render(request, 'index.html', {"shows" : shows, "movies" : movies})

@csrf_exempt
def get_episodes_from_name(request):
    name = request.POST.get('name')
    episodes = Content.objects.filter(channel=Channel.objects.get(title=name))
    return JsonResponse({
        "episodes" :  serializers.serialize('json', episodes),
        "status" : "ok"
    })

