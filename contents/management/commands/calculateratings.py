from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from contents.models import Channel
import os
import pandas as pd

class Command(BaseCommand):

    help = "Creates a csv with channel titles and ratings" 

    def _create_dataframe(self, channels):
        df = pd.DataFrame(columns=('channel title', 'average rating'))
        channel_ratings = []
        channel_titles = []
        for channel in channels:
            channel_ratings.append(channel.rating)
            channel_titles.append(channel.title)
        df['channel title'] = channel_titles
        df['average rating'] = channel_ratings
        return df.sort_values('average rating', ascending=False)

    
    def handle(self, *args, **kwargs):
        path = os.path.join(settings.MEDIA_ROOT, 'docs', 'channel_rating.csv')
        channels = Channel.objects.all()
        df = self._create_dataframe(channels)
        df.to_csv(path, index=False)
        self.stdout.write(
                self.style.SUCCESS(f'Successfully saved ratings in {path}')
            )