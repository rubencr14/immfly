from django.contrib import admin
from contents.models import Channel, Content

admin.site.register(Channel)
admin.site.register(Content)